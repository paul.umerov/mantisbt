package tests;

import com.jcraft.jsch.JSchException;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import steps.LoginSteps;
import steps.ManageUserSteps;

import utils.SSHService;
import utils.entities.User;

import java.sql.SQLException;
import java.util.ArrayList;

@RunWith(SerenityRunner.class)
public class Test1 {

    User user;

    @Managed
    WebDriver driver;

    @Steps
    LoginSteps loginSteps;
    @Steps
    ManageUserSteps manageUserSteps;


    @Before
    public void before() throws JSchException, ClassNotFoundException, SQLException {
        //Делаем SSH сессию
        SSHService.getSession();
        //Пользователем под которым логинимся
        user = new User();
        user.setName("administrator");
        user.setPassword("mantisbt2019");
    }

    @After
    public void after(){
        SSHService.close();
    }

    @Test
    @Title("Test Case N1:use one of the programming languages: Python/PHP using Selenium. But I use Java:)")
    public void test() throws SQLException {
        loginSteps.open();
        loginSteps.loginWithCredentials(user);
        manageUserSteps.open();
        manageUserSteps.setToShowHidden();
        ArrayList<User> actualList = manageUserSteps.getEnbaledUsers();
        manageUserSteps.getAllEnabledUsersFromDB(actualList);
    }
}
