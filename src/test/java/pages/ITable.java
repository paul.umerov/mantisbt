package pages;

import net.serenitybdd.core.pages.WebElementFacade;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public interface ITable {

    public default ArrayList<HashMap<String, WebElementFacade>> getRows(WebElementFacade tableHead, WebElementFacade tableBody){
        List<String> headers = tableHead.thenFindAll(".//th").stream().map(WebElementFacade::getText)
                .collect(Collectors.toList());
        ArrayList<HashMap<String, WebElementFacade>> rows = new ArrayList<>();
        tableBody.thenFindAll("./tr").stream().map(e->e.thenFindAll("./td")).collect(Collectors.toList())
                .forEach(i->{
                    HashMap<String, WebElementFacade> map = new HashMap<>();
                    Assert.assertEquals(headers.size(), i.size());
                    int x = 0;
                    for (String j:headers) {
                        map.put(j, i.get(x));
                        x++;
                    }
                    rows.add(map);
                });
        return rows;
        }


}

