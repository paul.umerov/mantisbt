package pages;

import net.serenitybdd.core.pages.WebElementFacade;
import org.openqa.selenium.support.FindBy;


public class PasswordPage extends BasePage {

    @FindBy(xpath = "//*[@id='password']")
    WebElementFacade passwordInput;

    @FindBy(xpath = "//*[@id=\"login-form\"]/fieldset/input[3]")
    WebElementFacade sumbitButton;

    public void enterPasswordAndSubmit(String password){
        passwordInput.type(password);
        sumbitButton.click();
    }
}
