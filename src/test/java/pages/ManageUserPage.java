package pages;

import lombok.Getter;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.HashMap;

@Getter
@DefaultUrl("http://HOST/manage_user_page.php")
public class ManageUserPage extends BasePage implements ITable{

    @FindBy(xpath = "//*[@id=\"main-container\"]//table/tbody")
    WebElementFacade usersTable;

    @FindBy(xpath = "//*[@id=\"main-container\"]//table/thead")
    WebElementFacade usersTableHeader;

    @FindBy(xpath = "//*[@id=\"manage-user-filter\"]/fieldset/label[2]/span")
    WebElementFacade showHiddenUsersCheckbox;

    @FindBy(xpath = "//*[@id=\"manage-user-filter\"]/fieldset/input[7]")
    WebElementFacade submitFilterButton;

    public ArrayList<HashMap<String, WebElementFacade>> getaAllUsersInfo(){
        return getRows(usersTableHeader, usersTable);
    }


}
