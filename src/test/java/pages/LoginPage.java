package pages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("http://HOST/login_page.php")
public class LoginPage extends BasePage {

    @FindBy(xpath = "//*[@id='username']")
    WebElementFacade userNameInput;

    @FindBy(xpath = "//*[@id=\"login-form\"]/fieldset/input[2]")
    WebElementFacade submitButton;

   public void enterLoginAndSubmit(String login){
       userNameInput.type(login);
       submitButton.click();
   }




}
