package steps;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.junit.Assert;
import org.openqa.selenium.By;
import pages.ManageUserPage;
import utils.DataBaseHelper;
import utils.SqlQueryHelper;
import utils.entities.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ManageUserSteps {

    ManageUserPage manageUserPage;

    @Step("Open users page")
    public void open(){
        manageUserPage.open();
    }

    @Step("Get all users from page")
    public ArrayList<User> getAllUsers(){
        ArrayList<User> users = new ArrayList<>();
        ArrayList<HashMap<String, WebElementFacade>> userListInfo =  manageUserPage.getaAllUsersInfo();
        userListInfo.stream().forEach(i->{
            User user = new User(i.get("E-mail").getText(), null);
            user.setName(i.get("Имя").getText());
            user.setUsername(i.get("Пользователь ").getText());
            user.setEnabled(i.get("Активен").containsElements(By.tagName("i")));
            user.setProtect(i.get("Защищен").containsElements(By.tagName("i")));
            users.add(user);
            }
        );
        return users;
    }


    @Step("Get enbaled users")
    public ArrayList<User> getEnbaledUsers(){
        return (ArrayList<User>) getAllUsers().stream().filter(u->u.getEnabled()==true).collect(Collectors.toList());
    }


    @Step("Show hidden users")
    public void setToShowHidden(){
        manageUserPage.getShowHiddenUsersCheckbox().click();
        manageUserPage.getSubmitFilterButton().click();
    }

    @Step("Assert that page contains all enabled users from the ‘mantis_user_table’ table")
    public void getAllEnabledUsersFromDB(List<User> userList) throws SQLException {
        ArrayList<String> actualList = (ArrayList<String>) userList.stream().map(i->i.getEmail()).collect(Collectors.toList());
        ArrayList<String> expectedList = DataBaseHelper.exectQuery("SELECT * FROM public.mantis_user_table where ENABLED = true", "email" );
        //Проверка контрольной суммы
        Assert.assertEquals("List size from DB not equals with list size from page", expectedList.size(), actualList.size());
        //Проверяем содержится ли пользователь в БД
        ArrayList<String> finalExpectedList = expectedList;
        actualList.stream().forEach(i->{
            Assert.assertTrue(String.format("User with mail %s not contains in public.mantis_user_table", i), finalExpectedList.contains(i));
        });
        actualList = (ArrayList<String>) userList.stream().map(i->i.getUsername()).collect(Collectors.toList());
        expectedList = DataBaseHelper.exectQuery("SELECT * FROM public.mantis_user_table where ENABLED = true", "username" );
        ArrayList<String> finalExpectedList1 = expectedList;
        actualList.stream().forEach(i->{
            Assert.assertTrue(String.format("User with username %s not contains in public.mantis_user_table", i), finalExpectedList1.contains(i));
        });
    }
}
