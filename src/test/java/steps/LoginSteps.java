package steps;

import net.thucydides.core.annotations.Step;
import pages.LoginPage;
import pages.PasswordPage;
import utils.entities.User;

public class LoginSteps {

    LoginPage loginPage;
    PasswordPage passwordPage;

    @Step("Open login page")
    public void open(){
        loginPage.open();
    }

    @Step("Login with credentials {0}")
    public void loginWithCredentials(User user){
        loginPage.enterLoginAndSubmit(user.getName());
        passwordPage.enterPasswordAndSubmit(user.getPassword());
    }
}
