package utils;

public class SqlQueryHelper {

    public String getCardAllDataByID(String uuid){
        return "select * from dsg_pensioner_card where id = '" + uuid +"'";
    }

    public String getPersonIdByPersonVersionId(String personVersionId){
        return "select person_id from dsg_person_version where id = '"+personVersionId+"'";
    }

    public String getQueryForSelectAllColumnNameInTable(String tabelName){
        String result =  "SELECT column_name \n" +
                "FROM INFORMATION_SCHEMA.COLUMNS \n" +
                "WHERE table_name = '"+tabelName.replaceAll(".*\\.",  "")+"'";
        return result;
    }

    public String getQueryForCountNotDeleted(String tableName){
        return "select count(*) from "+tableName+" where deleted != true";
    }

    public String getSqlForCardRegisterPage(String select,  String orderBy){
        //dsg_federal_subject.code desc, dsg_pensioner_card.card_number  desc
        return String.format("select distinct\n" +
                "dsg_pensioner_card.id,\n" +
                "%s\n" +
                "\n" +
                "from dsg_pensioner_card \n" +
                "\n" +
                "left join dsg_recruitment_office on dsg_pensioner_card.recruitment_office_id = dsg_recruitment_office.id\n" +
                "left join dsg_federal_subject on dsg_federal_subject.id = dsg_recruitment_office.federal_subject_id\n" +
                "left join dsg_person_version on dsg_pensioner_card.person_version_id = dsg_person_version.id\n" +
                "left join dsg_military_rank on dsg_pensioner_card.military_rank_id = dsg_military_rank.id\n" +
                "left join dsg_pension_type on dsg_pensioner_card.pension_type_id = dsg_pension_type.id\n" +
                "left join dsg_person on dsg_person_version.person_id = dsg_person.id\n" +
                "left join dsg_personal_document on dsg_personal_document.person_id = dsg_person.id\n" +
                "left join dsg_bso2 on dsg_bso2.pensioner_card_id = dsg_pensioner_card.id\n" +
                "\n" +
                "order by %s limit 50", select, orderBy);
    }


    public String getQueryForCount(String tableName){
        return "select count(*) from "+tableName;
    }


}
