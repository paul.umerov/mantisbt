package utils;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.hibernate.SessionFactory;

import java.sql.Connection;


public class SSHService {

    static Session session= null;

    public static Session getSession() throws SQLException {
        if(session==null){
            int lport=3307;
            String rhost="146.185.143.168";
            String host="146.185.143.168";
            int rport=3306;
            String user="root";
            String password="testQA2019";
            try{
                //Set StrictHostKeyChecking property to no to avoid UnknownHostKey issue
                java.util.Properties config = new java.util.Properties();
                config.put("StrictHostKeyChecking", "no");
                JSch jsch = new JSch();
                session=jsch.getSession(user, host, 22);
                session.setPassword(password);
                session.setConfig(config);
                session.connect();
                System.out.println("Connected");
                int assinged_port=session.setPortForwardingL(lport, rhost, rport);
                System.out.println("localhost:"+assinged_port+" -> "+rhost+":"+rport);
                System.out.println("Port Forwarded");
            }catch(Exception e){
                e.printStackTrace();
            }finally{
                if(session !=null && session.isConnected()){
                    System.out.println("Closing SSH Connection");
                    session.disconnect();
                }
            }
        }
        return session;
    }

    public static void close(){
        if(session !=null && session.isConnected()){
            System.out.println("Closing SSH Connection");
            session.disconnect();
        }
    }

}
