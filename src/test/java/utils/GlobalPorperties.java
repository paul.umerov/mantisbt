package utils;

import net.thucydides.core.util.SystemEnvironmentVariables;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Properties;

/**
 * Created by Paul on 30.03.2017.
 */
public class GlobalPorperties {
    private static LinkedList<Properties> propertiesList = null;

    GlobalPorperties() {}

    private static LinkedList<Properties> createProperties() throws IOException {
        LinkedList<Properties> propertiesList = new LinkedList<>();
        Properties system = SystemEnvironmentVariables.createEnvironmentVariables().getProperties();
        propertiesList.add(system);

//        String[] configFileNames = system.getProperty("config.location").split(";");
//        for(String fileName: configFileNames) {
//            Properties configuration = new Properties();
//            configuration.load(new FileInputStream(fileName));
//            propertiesList.addFirst(configuration);
//        }
        return propertiesList;
    }

    public static String getProperty(String property) throws IOException {
        if (propertiesList == null) {
            propertiesList = createProperties();
        }
        for(Properties properties: propertiesList) {
            String result = properties.getProperty(property, null);
            if(result != null) {
                return result;
            }
        }
        throw new IllegalArgumentException();

    }

    @SuppressWarnings("unchecked")
    public <T> T getPropertyGeneric(String property) throws IOException {
        return (T)getProperty(property);
    }


}

