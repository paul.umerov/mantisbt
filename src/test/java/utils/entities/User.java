package utils.entities;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;


@Getter@Setter
public class User{

    public static String tableName = "public.mantis_user_table";
    Integer id;
    String name;
    String password;
    String username;
    String email;
    Boolean enabled;
    Boolean protect;

    public User() {
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
