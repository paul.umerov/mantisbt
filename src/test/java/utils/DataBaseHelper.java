package utils;

import org.postgresql.util.PSQLException;

import java.io.IOException;
import java.sql.*;
import java.util.*;

public class DataBaseHelper {

    private static String getValueFromResultSet(String columnName, ResultSet resultSet) throws SQLException {
        int columnIndex = resultSet.findColumn(columnName);
        String value;
        int type = resultSet.getMetaData().getColumnType(columnIndex);
        if (type == Types.BIT || type == Types.BOOLEAN) {
            value = String.valueOf(resultSet.getBoolean(columnIndex));
        } else {
            value = resultSet.getString(columnIndex);
        }
        return value;
    }

    public static Map<String, Object> exectQuery(String dbPrefences, String sqlQurery, String[] fieldName) throws SQLException {
        Statement st = null;
        Map<String, Object> data = new HashMap<>();
        ResultSet rs = null;
        try {
            st = getPostqresConnection(dbPrefences).createStatement();
            rs = st.executeQuery(sqlQurery);
            while(rs.next()){
                for (String i: fieldName){
                    data.put(i, getValueFromResultSet(i, rs));
                }

            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            rs.close();
        }
        return data;

    }

    public static List<HashMap<String, Object>> selectList(String sqlQuery) throws SQLException {
        List<HashMap<String, Object>> rows = new ArrayList();
        try (Statement st = getPostqresConnection("defaultDB").createStatement())
        {
            try (ResultSet resultSet = st.executeQuery(sqlQuery)) {
                ResultSetMetaData md = resultSet.getMetaData();
                int columns = md.getColumnCount();

                while (resultSet.next()) {
                    HashMap<String, Object> row = new HashMap(columns);
                    for (int i = 1; i <= columns; ++i) {
                        row.put(md.getColumnName(i), resultSet.getObject(i));
                    }
                    rows.add(row);
                }
            } catch (PSQLException e)
            {

                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return rows;

    }

    public static List<HashMap<String, String>> selectListAsString(String sqlQuery) throws SQLException {
        List<HashMap<String, String>> rows = new ArrayList();
        try (Statement st = getPostqresConnection("defaultDB").createStatement())
        {
            try (ResultSet resultSet = st.executeQuery(sqlQuery)) {
                ResultSetMetaData md = resultSet.getMetaData();
                int columns = md.getColumnCount();

                while (resultSet.next()) {
                    HashMap<String, String> row = new HashMap(columns);
                    for (int i = 1; i <= columns; ++i) {
                        row.put(md.getColumnName(i), Objects.toString(resultSet.getObject(i), ""));
                    }
                    rows.add(row);
                }
            } catch (PSQLException e)
            {

                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return rows;

    }

    public static HashMap<String, String> getResultInMap(String sqlQuery) throws SQLException {

        HashMap<String, String> row = null;
        try (Statement st = getPostqresConnection("defaultDB").createStatement())
        {
            try (ResultSet resultSet = st.executeQuery(sqlQuery)) {
                ResultSetMetaData md = resultSet.getMetaData();
                int columns = md.getColumnCount();

                while (resultSet.next()) {
                  row = new HashMap<String, String>(columns);
                    for (int i = 1; i <= columns; ++i) {
                        row.put(md.getColumnName(i), Objects.toString(resultSet.getObject(i), ""));
                    }

                }
            } catch (PSQLException e) {

                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return row;

    }

    //Получаем данные из дефолтноной БД
    //Дефолтная БД указана в serenity.properties как defaultDB
    public static HashMap<String, String> exectQuery(String sqlQurery, Iterable<String> fieldName) throws SQLException {
        Statement st = null;
        String dbPrefences = "defaultDB";
        HashMap<String, String> data = new HashMap<>();
        ResultSet rs = null;
        try (Connection connection = getPostqresConnection(dbPrefences)){
            st = connection.createStatement();
            rs = st.executeQuery(sqlQurery);
            while(rs.next()){
                for (String i: fieldName){
                    data.put(i, getValueFromResultSet(i, rs));
                }

            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return data;

    }

    //Метод выбирает одно значение из массива результатов
    static public ArrayList<String> exectQuery(String sqlQurery, String fieldName) throws SQLException {
        Statement st = null;
        String dbPrefences = "defaultDB";
        ArrayList<String> values = new ArrayList<>();
        try (Connection connection = getPostqresConnection(dbPrefences)) {
            st = connection.createStatement();
            ResultSet rs  = st.executeQuery(sqlQurery);
            while(rs.next()){
                values.add(getValueFromResultSet(fieldName, rs));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return values;

    }

    //Метод просто выполняет запрос
    static public void exectQuery(String sqlQurery) throws SQLException {
        Statement st = null;
        String dbPrefences = "defaultDB";
        ArrayList<String> values = new ArrayList<>();
        boolean rs = false;
        try (Connection connection = getPostqresConnection(dbPrefences)) {
            st = connection.createStatement();
            rs = st.execute(sqlQurery);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    static Connection getPostqresConnection(String dbPrefences) throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        String[] dbPref = new String[0];
        try {
            dbPref = GlobalPorperties.getProperty("defaultDB").split("&");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Connection connection = DriverManager.getConnection(dbPref[0], dbPref[1], dbPref[2]);

        return connection;
    }

    public static ArrayList<String> getAllColumnNameInTable(String tableName) throws SQLException {
        ArrayList<String> names = new DataBaseHelper()
                .exectQuery(new SqlQueryHelper().getQueryForSelectAllColumnNameInTable(tableName), "column_name");
        return names;
    }




}
